# Contributing

* We are open to issues, feature requests, and pull requests.
* Before submitting a PR, please run `cargo fmt && cargo clippy`, and fix any issues that clippy raises.
* New code will not be accepted without accompanying tests.
* Please document all additional items with doc comments, including private items.
* When developing, run `cargo doc --document-private-items` to see all documentation.
