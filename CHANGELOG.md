# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog][keep], and this project
adheres to [Semantic Versioning][semver].

## [Unreleased]

## [0.3.4] - 2020-04-21

### Fixed

- [Issue #3][issue-3]: Fix panic when given `..` or `../..` path on Windows ([PR !2][pr-2])

[issue-3]: https://gitlab.com/avandesa/ichwh-rs/-/issues/3
[pr-2]: https://gitlab.com/avandesa/ichwh-rs/-/merge_requests/2

## [0.3.3] - 2020-04-18

### Fixed

- [Issue #2][issue-2]: On Windows, count local executables without backslashes

[issue-2]: https://gitlab.com/avandesa/ichwh-rs/-/issues/2



## [0.3.2] - 2020-04-12

### Fixed

- [PR !1][pr-1] (@jonathandturner): On Windows, count symlinks as executables

[pr-1]: https://gitlab.com/avandesa/ichwh-rs/-/merge_requests/1

## [0.3.1] - 2020-01-30

### Fixed

- [Issue #1][issue-1]: Fix bug where a non-existent directory on `$PATH` would cause search to fail

[issue-1]: https://gitlab.com/avandesa/ichwh-rs/issues/1

## [0.3.0] - 2020-01-22

### Changed

- (BREAKING CHANGE) `which` returns an `IchwhResult<Option<_>>`, and the
  `IchwhError::BinaryNotFound` variant is removed
- `which` and `which_all` are now internally concurrent, potentially providing a small speedup

## [0.2.2] - 2020-01-13

### Added

- `symlink_chain` and `symlink_chain_in_dir` functions, which find a binary, then follows that
  binary's chain of symbolic links.

## [0.2.1] - 2019-12-30

### Added

- Identify local files (e.g., `which ./target/debug/foo`)

## [0.2.0] - 2019-12-30

### Fixed

- Fix type of `IchwhResult`

## [0.1.3] - 2019-12-30

### Added

- Error enum field for when PATHEXT is not defined

### Fixed

- Ignore case when comparing filenames on Windows
- Respect order of PATHEXT on Windows
- Return all matches in all PATH directories in `which_in_all` on Windows

## [0.1.2] - 2019-12-29

### Added

- Support for Windows

## [0.1.1] - 2019-12-26

### Added

- `which_all`

## [0.1.0] - 2019-12-26

### Added

- `which`
- `which_in_dir`

[Unreleased]: https://gitlab.com/avandesa/ichwh-rs/compare/v0.3.0...master
[0.1.0]: https://gitlab.com/avandesa/ichwh-rs/tags/v0.1.0
[0.1.1]: https://gitlab.com/avandesa/ichwh-rs/compare/v0.1.0...v0.1.1
[0.1.2]: https://gitlab.com/avandesa/ichwh-rs/compare/v0.1.1...v0.1.2
[0.1.3]: https://gitlab.com/avandesa/ichwh-rs/compare/v0.1.2...v0.1.3
[0.2.0]: https://gitlab.com/avandesa/ichwh-rs/compare/v0.1.3...v0.2.0
[0.2.1]: https://gitlab.com/avandesa/ichwh-rs/compare/v0.2.0...v0.2.1
[0.2.2]: https://gitlab.com/avandesa/ichwh-rs/compare/v0.2.1...v0.2.2
[0.3.0]: https://gitlab.com/avandesa/ichwh-rs/compare/v0.2.2...v0.3.0
[0.3.1]: https://gitlab.com/avandesa/ichwh-rs/compare/v0.3.0...v0.3.1
[0.3.2]: https://gitlab.com/avandesa/ichwh-rs/compare/v0.3.1...v0.3.2
[0.3.3]: https://gitlab.com/avandesa/ichwh-rs/compare/v0.3.2...v0.3.3
[0.3.4]: https://gitlab.com/avandesa/ichwh-rs/compare/v0.3.3...v0.3.4

[keep]: https://keepachangelog.com/en/1.0.0
[semver]: https://semver.org/spec/v2.0.0.html
