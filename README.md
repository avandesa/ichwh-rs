# `ichwh` - an async implementation of GNU `which`

[![Crates.io](https://img.shields.io/crates/v/ichwh)][cratesio]
[![Gitlab pipeline status](https://img.shields.io/gitlab/pipeline/avandesa/ichwh-rs)][pipeline]
[![Docs.rs](https://docs.rs/ichwh/badge.svg)][docs]
[![AppVeyor](https://img.shields.io/appveyor/ci/avandesa/ichwh-rs)][appveyor]

`ichwh` aims to be a fully-async clone of [GNU `which`][gnu], compatible with the three major
operating systems. The main job of `which` is to search for executables on the current `PATH`.

This crate is similar to the synchronous [`which`][which-sync-crate] crate. The main difference is
that `ichwh` is asynchronous, and provides a `which_all` method.

[cratesio]: https://crates.io/crates/ichwh
[pipeline]: https://gitlab.com/avandesa/ichwh-rs/pipelines
[appveyor]: https://ci.appveyor.com/project/avandesa/ichwh-rs

[gnu]: https://carlowood.github.io/which/
[which-sync-crate]: https://crates.io/crates/which

## Usage

In `Cargo.toml`:

```toml
ichwh = "*"
```

In your source files:

```rust
use ichwh::which;

let path_to_python = which("python").await.unwrap();
assert_eq!(path_to_python.to_str().unwrap(), "/usr/bin/python");
```

## Testing

Run tests with `cargo test`. The tests will create a set of temporary test folders. They are rooted
at `/tmp/ichwh-test-<num>` (`%TEMP%/...` on windows), where `<num>` is the number of minutes since
the unix epoch. If all tests pass, the folders will be cleaned up. Any failing tests will preserve
their test folders for inspection and debugging.

## [Changelog](/CHANGELOG.md)

## Versioning

This crate adheres to [SemVer]. This crate is pre-1.0, meaning that breaking changes to the
publicly-documented API will bump the minor version, and non-breaking changes will bump the patch
version.

All publicly-documented items in this crate (i.e., items visible for a platform on [docs.rs][docs])
are considered stable until a breaking version increment. Do not depend on or use non-documented
items that may be visible in-code; they may change at any time.

Please report any unexpected API breakage as a bug.

[docs]: https://docs.rs/ichwh
[SemVer]: https://semver.org/spec/v2.0.0.html

## Contributing

See CONTRIBUTING.md.
