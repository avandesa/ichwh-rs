//! Macros and functions used by both the unix and windows tests

/// Shortcut for creating a `PathBuf` out of fragments
#[macro_export]
macro_rules! path {
    ($($fragments:expr),+) => {
        [$($fragments),+].iter().collect::<::async_std::path::PathBuf>()
    };
}

/// Shortcut for creating a temporary test folder. Returns a path to the folder.
#[macro_export]
macro_rules! build_test_folder {
    ($subdir:literal) => {
        async {
            #[cfg(unix)]
            let root = String::from("/tmp");
            #[cfg(windows)]
            let root = ::std::env::var("TEMP").unwrap();

            use ::std::time::SystemTime;
            let time = SystemTime::now()
                .duration_since(SystemTime::UNIX_EPOCH)
                .unwrap();
            let folder_name = format!("ichwh-test-{}", time.as_secs() / 60);
            let full_path = path!(&root, &folder_name, $subdir);

            if full_path.exists().await {
                ::async_std::fs::remove_dir_all(&full_path).await.unwrap();
            }

            ::async_std::fs::create_dir_all(&full_path).await.unwrap();

            full_path
        }
    };
}

/// Creates a new directory in the given directory, returning a path to the created directory
#[macro_export]
macro_rules! build_subdir {
    ($root:expr, $subdir:literal) => {
        async {
            let path = path!($root.to_str().unwrap(), $subdir);

            ::async_std::fs::create_dir_all(&path).await.unwrap();

            path
        }
    };
}

/// Creates a file in the given directory with the given name, and returns a handle to the file.
#[macro_export]
macro_rules! create_file {
    ($dir:expr, $filename:literal) => {
        async {
            let file_path = path!($dir.to_str().unwrap(), $filename);
            ::async_std::fs::File::create(file_path).await.unwrap()
        }
    };
}

/// Removes all files & subdirectories in the given test directory. If there are no more test dirs
/// in the test root, removes the test root.
pub async fn cleanup_test_dir<P: AsRef<async_std::path::Path>>(dir: P) {
    // Remove the directory for this test
    async_std::fs::remove_dir_all(&dir).await.unwrap();

    // `remove_dir` will return `Err` if the directory is not empty, but we just ignore it.  The
    // last test to finish will remove the root test dir. If a test fails, then this function won't
    // be called, and the dir won't be cleaned up. This can be useful for debugging the tests.
    let parent = &dir.as_ref().parent().unwrap();
    let _ = async_std::fs::remove_dir(&parent).await;
}
