//! Tests
//!
//! Tests that search the filesystem should use a temporary test directory for the specific test,
//! and the directory should be cleaned up after the test is finished, unless the test fails.
//! Obtain a path to the test directory with the `build_test_folder!` macro. Create files and
//! symlinks with the `create_exec_file!`, `create_noexec_file!`, and `create_symlink!` macros.
//! When the test is complete, clean up the directory with the `cleanup_test_dir` function.

mod unix_macros;

use {super::common_macros::cleanup_test_dir, crate::*, std::env::set_var};

/// Tests that one binary among many files can be found
#[async_std::test]
async fn binary_among_many() {
    let dir = build_test_folder!("binary-among-many").await;

    create_exec_file!(dir, "foo").await;
    create_noexec_file!(dir, "bar").await;
    create_noexec_file!(dir, "baz").await;

    // `.../test_root/binary-among-many/foo`
    let expected_path = path!(dir.to_str().unwrap(), "foo");
    let path = which_in_dir("foo", &dir).await.unwrap();

    assert_eq!(path, Some(expected_path));

    cleanup_test_dir(&dir).await;
}

/// Test that though a filename may match, it must be executable to be returned
#[async_std::test]
async fn file_not_executable() {
    let dir = build_test_folder!("file-not-executable").await;

    create_noexec_file!(dir, "foo").await;
    create_noexec_file!(dir, "bar").await;
    create_noexec_file!(dir, "baz").await;

    let path = which_in_dir("bar", &dir).await.unwrap();

    assert_eq!(path, None);

    cleanup_test_dir(&dir).await;
}

/// Tests that symlinks to executable files are returned
#[async_std::test]
async fn symlink_to_executable() {
    let dir = build_test_folder!("symlink-to-executable").await;

    create_exec_file!(dir, "foo").await;
    create_noexec_file!(dir, "bar").await;
    create_noexec_file!(dir, "baz").await;

    // Link `qux` to `foo`; `qux` should then be 'executable'
    create_symlink!(&dir, "foo", "qux").await;

    // `.../test_root/symlink-to-executable/qux`
    let expected_path = path!(dir.to_str().unwrap(), "qux");
    let path = which_in_dir("qux", &dir).await.unwrap();

    assert_eq!(path, Some(expected_path));

    cleanup_test_dir(&dir).await;
}

#[async_std::test]
async fn local_file() {
    let dir = build_test_folder!("local-file").await;

    let cwd = std::env::current_dir().unwrap();
    std::env::set_current_dir(&dir).unwrap();

    create_exec_file!(dir, "foo").await;
    let expected_path = Some(path!(dir.to_str().unwrap(), "foo"));

    assert_eq!(which("./foo").await.unwrap(), expected_path);
    assert_eq!(which("./foo").await.unwrap(), expected_path);
    assert_eq!(which("../local-file/foo").await.unwrap(), expected_path);

    std::env::set_current_dir(&cwd).unwrap();
    cleanup_test_dir(&dir).await;
}

#[async_std::test]
async fn executable_dir() {
    let dir = build_test_folder!("executable-dir").await;
    create_exec_dir!(dir, "foo").await;

    let path = which_in_dir("foo", &dir).await.unwrap();
    assert_eq!(path, None);

    cleanup_test_dir(&dir).await;
}

#[async_std::test]
async fn symlink_chain_test() {
    let dir = build_test_folder!("symlink-chain-test").await;

    // qux -> baz -> bar -> foo
    create_exec_file!(dir, "foo").await;
    create_symlink!(dir, "foo", "bar").await;
    create_symlink!(dir, "bar", "baz").await;
    create_symlink!(dir, "baz", "qux").await;

    let expected_chain = vec![
        path!(dir.to_str().unwrap(), "qux"),
        path!(dir.to_str().unwrap(), "baz"),
        path!(dir.to_str().unwrap(), "bar"),
        path!(dir.to_str().unwrap(), "foo"),
    ];

    let chain = symlink_chain_in_dir("qux", &dir).await.unwrap();
    assert_eq!(chain, expected_chain);

    let expected_chain = vec![path!(dir.to_str().unwrap(), "foo")];

    let chain = symlink_chain_in_dir("foo", &dir).await.unwrap();
    assert_eq!(chain, expected_chain);

    cleanup_test_dir(&dir).await;
}

/// Tests the core `which` function, which searches the path
#[async_std::test]
async fn path_searches() {
    let dir = build_test_folder!("path-searches").await;

    let a = build_subdir!(&dir, "a").await;
    create_exec_file!(a, "foo").await;
    create_exec_file!(a, "bar").await;

    let b = build_subdir!(&dir, "b").await;
    create_exec_file!(b, "bar").await;

    let c = build_subdir!(&dir, "c").await;
    create_symlink!(b, "baz", c, "qux").await;

    // Ensure it still works with a nonexistent folder on the path
    let nonexistent = path!(dir.to_str().unwrap(), "not_real");

    // Set the PATH variable to this test
    let path_var = format!(
        "{}:{}:{}:{}",
        nonexistent.to_str().unwrap(),
        a.to_str().unwrap(),
        b.to_str().unwrap(),
        c.to_str().unwrap()
    );
    dbg!(&path_var);
    set_var("PATH", &path_var);

    // `.../test_root/path-searches/a/foo`
    let expected_path = Some(path!(a.to_str().unwrap(), "foo"));
    let path = which("foo").await.unwrap();
    assert_eq!(path, expected_path);

    // `.../test_root/path-searches/b/baz`
    let expected_path = Some(path!(a.to_str().unwrap(), "bar"));
    let path = which("bar").await.unwrap();
    assert_eq!(path, expected_path);

    // `.../test_root/path-searches/c/qux`
    let expected_path = Some(path!(c.to_str().unwrap(), "qux"));
    let path = which("qux").await.unwrap();
    assert_eq!(path, expected_path);

    // Test `which_all`
    let expected_paths = vec![
        path!(a.to_str().unwrap(), "bar"),
        path!(b.to_str().unwrap(), "bar"),
    ];
    let paths = which_all("bar").await.unwrap();
    assert_eq!(paths, expected_paths);

    cleanup_test_dir(&dir).await;
}
