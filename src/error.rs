//! Specialized Error and Result types
//!
//! `ichwh` uses [`thiserror`] to define a specialized error type that provides context on what
//! went wrong. This module also contains a typedef for `Result` that uses `IchwhError` as the
//! error type.
//!
//! [`thiserror`]: https://crates.io/crates/thiserror

use thiserror::Error;

/// A specialized result type for the crate
pub type IchwhResult<T> = std::result::Result<T, IchwhError>;

/// A specialized error type for the crate.
#[derive(Error, Debug)]
pub enum IchwhError {
    /// Indicates that the PATH variable was not defined in the environment
    #[error("PATH was not defined in environment")]
    PathNotDefined,

    /// Wrapper for a generic I/O error
    #[error("an io error occured: {0:?}")]
    IoError(#[from] std::io::Error),

    /// Indicates that the PATHEXT variable was not defined in the environment
    #[cfg(windows)]
    #[error("PATHEXT was not defined in environment")]
    PathextNotDefined,
}
