//! `cargo run --release --example cli -- ls python asdf`

use ichwh::*;

#[async_std::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    for arg in std::env::args().skip(1) {
        if let Some(path) = which(&arg).await? {
            println!("Location of {}: {}", arg, path.to_str().unwrap());
        } else {
            println!("Binary not found: {}", arg);
        }
    }

    Ok(())
}
